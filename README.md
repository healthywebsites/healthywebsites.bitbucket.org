# Outline

We have 2 case studies to work through:
http://www.ctc.org.uk/
http://www.healthywebsites.co.uk/work/cycling-touring-club.html

After the CTC case study we wrote up a whitepaper on optimising Drupal: http://www.healthywebsites.co.uk/optimising-drupal.html

Worked on a Charity Project: http://www.5x50.org/ http://www.healthywebsites.co.uk/work/5x50.html

Which involved using Amazon Web Services to utilise a Load Balancer with multiple front end vps' and RDS for the backend database. Also made use of:

- php-fpm
- APC
- (memcached) ElastiCache for sessions and cache 
- NFS with cachefilesd
- Performance problem with logged in users adding content in peak times and how did we solve it
- Deploying updates with Capistrano without any downtime
- drupal-cap and how to use it
